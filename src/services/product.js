const Products = require('../models/product');


module.exports = {
    getAllProducts : async (req,res)=>{
        try {
            const product = await Products.find();
            return{
                message: 'list products',
                data : product
            }
        } catch (error) {
            console.error(error);
            throw new error('Server error!')
        }
    },
    getOneProduct : async (userId) =>{
        try {
          const getOne = await Products.findById(userId)  
          return getOne
        } catch (error) {
            console.log(error);
            throw new error(error)
        }
    },
    createOneProduct : async (title, desc, stock, price, imagePath, sellerId)=>{
        try {
            const createOne = await Products.create({
               title,
               desc,
               stock,
               price,
               image : imagePath,
               seller : sellerId 
            })
            return createOne
        } catch (error) {
            console.error(error)
        }
    },
    editOneProduct: async (productId, title, desc, stock, price, imagePath, userId) => {
        try {
          const product = await Products.findById(productId);
          console.log(product);
          if (!product) {
            return 'Product not found';
          } else if (product.seller.toString() !== userId) {
            return 'You are not authorized to edit this product';
          }
          const updatedProduct = await Products.findByIdAndUpdate(
            productId,
            {
              $set: {
                title,
                desc,
                stock,
                price,
                image: imagePath,
              },
            },
            { new: true }
          );
          return updatedProduct;
        } catch (error) {
          console.error(error);
          throw new Error('Internal server error');
        }
      }
      ,
    deleteProduct : async(sellerId)=>{
        try {
            const deleteProduct = await Products.findByIdAndDelete(sellerId)
            return {
                message : 'Produk berhasil di delete!',
            }
        } catch (error) {
            console.error(error)
        }
    }
}