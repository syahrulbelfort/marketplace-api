const bcrypt = require('bcryptjs');
const Seller = require('../models/sellers');
const { registerValidator, loginValidator } = require('../utils/validation');
const jwt = require('jsonwebtoken');

module.exports = {
  registerSeller: async (userData, imagePath) => {
    const { error } = registerValidator(userData);
    if (error) {
      return error.details[0].message;
    }

    const emailExist = await Seller.findOne({ email: userData.email });
    if (emailExist) {
      return 'Email already exists!';
    }

    const salt = await bcrypt.genSalt(10);
    const hashPassword = await bcrypt.hash(userData.password, salt);
    const { name, email } = userData;
    const seller = new Seller({
      name,
      email,
      password: hashPassword,
      image : imagePath
    });

    const saveSeller = await seller.save();
    
    return {
      seller: saveSeller._id,
      success: 'Registration successful!',
    };
  },
  loginSeller: async (userData) => {
    const { error } = loginValidator(userData);
    if (error) {
      throw new Error(error.details[0].message);
    }

    const email = { email: userData.email };
    const seller = await Seller.findOne(email);
    if (!seller) {
      return {
        status: 400,
        message: 'Email is not found!',
      };
    }

    const validPass = await bcrypt.compare(userData.password, seller.password);
    if (!validPass) {
      return {
        status: 400,
        message: 'Wrong password',
      };
    }

    const payload = {
      id: seller._id,
    };

    const token = jwt.sign(payload, process.env.TOKEN_SECRET);
    return {
      header: 'auth-token',
      token: token,
    };
  },
};
