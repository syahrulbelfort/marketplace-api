const mongoose = require('mongoose');

// Skema untuk seller
const SellerSchema = new mongoose.Schema({
  name: {
    type: String,
    required: true,
    min: 6,
    max: 255
  },
  email: {
    type: String,
    required: true,
    unique: true,
    min: 6,
    max: 255
  },
  password: {
    type: String,
    required: true,
    min: 6,
    max: 255
  },
  image: {
    type: String,
    required: true,
  },
  products: [{
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Product'
  }],
  created_at: {
    type: Date,
    default: Date.now
  }
});

const Seller = mongoose.model('Seller', SellerSchema);


module.exports= Seller
