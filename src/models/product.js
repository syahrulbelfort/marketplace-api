const mongoose = require('mongoose')
const {Schema} = mongoose

// Skema untuk produk
const ProductSchema = new Schema({
    title: {
      type: String,
      required: true,
      min:6
    },
    desc: {
      type: String,
      required: true,
      min:10
    },
    stock: {
      type: Number,
      required: true
    },
    price: {
      type: Number,
      required: true
    },
    image: {
      type: String,
      required: true
    },
    seller: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'Seller',
      required: true
    },
    created_at: {
      type: Date,
      default: Date.now
    }
  });

  const products = mongoose.model('Products', ProductSchema)

  module.exports= products
  