const {registerSeller, loginSeller} = require('../services/seller');

module.exports ={
    Register : async (req,res)=>{
        try {
            const result = await registerSeller(req.body, req.file.path);
            res.send(result)
            console.log(result);
        } catch (error) {
            console.error(error)
        }
    },
    Login : async (req,res)=>{
        try {
            const result = await loginSeller(req.body)
            res.send(result)
        } catch (error) {
            console.error(error)
        }
    }
}