const product = require('../services/product');

module.exports = {
  getAll: async (req, res) => {
    const result = await product.getAllProducts();
    res.send(result);
  },
  getOne: async (req, res) => {
    const result = await product.getOneProduct(req.params.id);
    res.send(result);
  },
  createProduct: async (req, res) => {
    const { title, desc,stock, price } = req.body;
    const result = await product.createOneProduct(
      title,
      desc,
      stock,
      price,
      req.file.path,
      req.user.id
    );
    res.send(result);
  },
  editProduct: async (req, res) => {
    const { title, desc, stock, price } = req.body;
    const result = await product.editOneProduct(
      req.params.id,
      title,
      desc,
      stock,
      price,
      req.file.path,
      req.user.id
    );
    res.send(result);
  },
  deleteProduct: async (req, res) => {
    const result = await product.deleteProduct(req.params.id);
    res.status(200).json({
      message: result
    });
  }
};
