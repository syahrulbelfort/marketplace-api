const router = require('express').Router();
const seller = require('../controller/seller');
const { verify } = require('jsonwebtoken');
const upload  = require('../middleware/multer')

router.post('/register', upload, seller.Register );
router.post('/login',verify, seller.Login );
  

module.exports = router