const router = require('express').Router();
const upload = require('../middleware/multer');
const verify = require('../middleware/verifyToken');
const product = require('../controller/product');

router.get('/', verify, product.getAll);
router.get('/:id', product.getOne);
router.post('/', verify, upload, product.createProduct);
router.put('/:id', verify, upload, product.editProduct);
router.delete('/:id', product.deleteProduct);

module.exports = router;
