const jwt = require('jsonwebtoken');

function authToken(req,res,next){
    const token = req.header('auth-token')
    if(!token) return res.status(401).send('Akses ditolak! Login terlebih dahulu')

    try {
        const verified = jwt.verify(token, process.env.TOKEN_SECRET)
        req.user =verified
        next()
    } catch (error) {
        res.status(401).send('Token tidak sesuai')
    }
}

module.exports= authToken