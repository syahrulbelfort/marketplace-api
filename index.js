require('dotenv').config()
const express = require('express');
const app = express();
const mongoose = require('mongoose');
const PORT = process.env.PORT
const methodOverride = require('method-override')
const sellerRoute = require('./src/routes/seller');
const productRoute = require('./src/routes/product')


//middleware
app.use(express.json())
app.use(methodOverride(`_method`))
app.use('/image',express.static('images'))


//connect to DB
async function connectDB(){
    try {
        await mongoose.connect(process.env.SECRET_URL,
            {
            useNewUrlParser: true,
           },
        console.log(`database connected!`)
        )
    } catch (error) {
        console.error(error)
    }
}

connectDB()

// route
app.use('/api/v1/product', productRoute)
app.use('/api/v1/seller', sellerRoute )

app.listen(PORT, ()=>{
    console.log(`server is running on ${PORT}`)
})
